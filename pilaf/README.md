## pilaf's maps

All maps made using GIMP.

### Axiom

Meant to be played in mirrored (expand map) mode.

Palette: Liero 1.33

Preferred mod: Promode

### Oath

Best played in mirrored (expand map) mode, but can work in normal mode too.

Palette: Liero 1.33

Preferred mod: Promode

### Hole

Meant to be player in non-mirrored mode.

Palette: Liero 1.33

Preferred mod: Promode

### G3A

Port from [Gusanos 3 Arena](https://onether.com/work/gusanos-3-arena) (Gusanos TC).

Meant to be played in non-mirrored mode.

Palette: Liero 1.33

Preferred mod: Promode
