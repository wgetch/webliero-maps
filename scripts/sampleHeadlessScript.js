let room = WLInit({});
let pools = await import("https://webliero.gitlab.io/webliero-maps/scripts/pools.js");
await pools.setPool("pools/test.json");
pools.shuffle();
room.onGameEnd2 = () => pools.loadNextMap(room);
